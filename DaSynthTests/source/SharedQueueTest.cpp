#include "catch.hpp"
#include "utils/SharedQueue.hpp"
#include <thread>

// Helper functions
void initData(std::vector<float> &data, const uint32_t initSize)
{
	data[0] = 1.5f;

	for (uint32_t i = 1; i < initSize; ++i)
	{
		data[i] = 1.0f;
	}

	data[initSize - 1] = 1.5f;
}

void initData(std::vector<float> &data, const uint32_t initSize, float value)
{
	for (uint32_t i = 0; i < initSize; ++i)
	{
		data[i] = value;
	}
}

void verifyData(float *dataSet1, float *dataSet2, uint16_t dataSetSize)
{
	for (uint16_t i = 0; i < dataSetSize; ++i)
	{
		REQUIRE(dataSet1[i] == dataSet2[i]);
	}
}

void Consumer(Synt::Core::SharedQueue< std::vector<float> > &sharedBuf)
{
	uint32_t recvVal= 0;
	for (int i = 0; i < 100; ++i) 
	{
			std::vector<float> data(256);
			sharedBuf.Pop(data);

			for (size_t j = 0; j < data.size();++j)
			{
				REQUIRE(data[j] == static_cast<float>(recvVal % 2));
			}
			recvVal++;
	}
}

void Producer(Synt::Core::SharedQueue< std::vector<float> > &sharedBuf)
{
	int32_t sentVal = 0;
	for (int i = 0; i < 100; ++i) 
	{
			std::vector<float> data(256);
			initData(data, data.size(), static_cast<float>(sentVal % 2));
			sharedBuf.Push(data);
			sentVal++;
	}
}

TEST_CASE("SharedBuffer api tests", "[SharedBuffer]") 
{
	const uint32_t size = 256;
	Synt::Core::SharedQueue< std::vector<float> > sharedBuf(size);
	std::vector<float> data(size);
	initData(data, size);

	REQUIRE(sharedBuf.GetBufferSize() == size);

	SECTION("Testing Concurrency")
	{
		std::thread t1(Producer, std::ref(sharedBuf));
		std::thread t2(Consumer, std::ref(sharedBuf));
		t1.join();
		t2.join();
	}
}
