#include "catch.hpp"
#include "cmn/Pipeline.hpp"
#include "utils/Pipe.hpp"
#include "cmn/Generator.hpp"
#include "cmn/SinOscillatorSource.hpp"
#include "cmn/StageDefs.hpp"

TEST_CASE("Pipeline api tests", "[Pipeline]") 
{
	using namespace Synt::Core;

	const uint32_t pipeSize = 512;
	Pipeline pipeline(pipeSize);
	PipePtr<std::vector<AmplitudeVal>> pipe = pipeline.CreatePipe();

	SECTION("Create a pipe")
	{
		auto sharedBuf = pipe->SharedBuffer();

		REQUIRE(sharedBuf->GetBufferSize() == pipeSize);
	}

	SECTION("Create a stage") 
	{
		std::shared_ptr< Generator<SinOscillatorSource> > genStage = 
			pipeline.AddStage<Generator, SinOscillatorSource>(SinOscillatorSource::Params(46));
		REQUIRE(genStage);
		uint32_t inputCnt = genStage->InputCount();
		uint32_t outputCnt = genStage->OutputCount();
		
		REQUIRE(inputCnt == 0);
		REQUIRE(outputCnt == 1);
	}
}
