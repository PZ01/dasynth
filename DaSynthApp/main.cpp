#include <iostream>
#include "cmn/Pipeline.hpp"
#include "cmn/Generator.hpp"
#include "cmn/SinOscillatorSource.hpp"
#include "io/OutputStreamer.hpp"
#include "io/WaveFileWriter.hpp"
#include "cmn/NoiseSource.hpp"
#include "cmn/FMSource.hpp"
#include "cmn/AMSource.hpp"
#include "cmn/Filter.hpp"
#include "cmn/LowPassFilter.hpp"

//#define CLI_MAIN

#if defined(CLI_MAIN)

#include <iostream>
#include <string>

int main()
{
	using namespace Synt::Core;
	using namespace std;

	Pipeline pl(256);

	cout << string(80, '*') << endl;
	cout << "Il n'y a pas de validation des donn\202es en entr\202es." << endl;
	cout << "Sample rate is set to 44.1khz and duration 5 seconds." << endl;
	cout << "Choose a generator[1/2] :" << endl;
	cout << "\t1-Sin Generator" << endl;
	cout << "\t2-Noise Generator" << endl;
	cout << "\t3-FM Generator" << endl;
	cout << "\t4-AM Generator" << endl;
	uint16_t genNum;
	cin >> genNum;

	StagePtr generator;

	switch (genNum)
	{
	case 1:
		int32_t tone ;
		cout << "Enter tone, value between 12 and 100(46 = C#)" << endl;
		cin >> tone;
		generator = pl.AddStage<Generator, SinOscillatorSource>(SinOscillatorSource::Params(tone));
		break;
	case 2:
		generator = pl.AddStage<Generator, NoiseSource>(NoiseSource::Params());
		cout << "Noise source has been set." << endl;
		break;
	case 3:
		generator = pl.AddStage<Generator, FMSource>(FMSource::Params(12, 2, 2));
		cout << "Frequency Modulation source has been set with tone of 12." << endl;
		break;
	case 4:
		generator = pl.AddStage<Generator, AMSource>(AMSource::Params(42, 2, 0.8f));
		cout << "Amplitude modulation has been set with a tone of 42." << endl;
		break;
	default:
		cout << "Wrong option, exiting ... " << endl;
		return 0;
	}

	StagePtr linearEnv;
	StagePtr multiplier;

	cout << "Apply a linear envelope?[y/Y/n/N]" << endl;
	char ynEnv;
	cin >> ynEnv;

	PipePtr<std::vector<AmplitudeVal>> pipe = pl.CreatePipe();

	if (ynEnv == 'y' || ynEnv == 'Y')
	{
		linearEnv = pl.AddStage<LinearEnvelope>(LinearEnvelope::Params(0.2f,0.4f,0.8f));
		multiplier = pl.AddStage<Multiplier>(Multiplier::Params());

		auto pipe2 = pl.CreatePipe();
		auto pipe3 = pl.CreatePipe();

		generator->SetOutputPipe(0, pipe);
		linearEnv->SetOutputPipe(0, pipe2);
		multiplier->SetInputPipe(0, pipe);
		multiplier->SetInputPipe(1, pipe2);
		multiplier->SetOutputPipe(0, pipe3);
		cout << "Linear envelope with an attack of 0.2s and a release of 0.4s has been set." << endl;
	}
	else
	{
		generator->SetOutputPipe(0, pipe);
	}

	StagePtr lowPassFilter;

	cout << "Apply a low pass filter?[y/Y/n/N]" << endl;
	char ynFilter;
	cin >> ynFilter;

	if (ynFilter == 'y' || ynFilter == 'Y')
	{
		float freqCutOff;
		cout << "Enter Freq CutOff(Less than Nyquist... ~22k), float : " << endl;
		cin >> freqCutOff;
		lowPassFilter = pl.AddStage<FilterStage, LowPassFilter>(LowPassFilter::Params(freqCutOff));

		if (linearEnv && multiplier)
		{
			lowPassFilter->SetInputPipe(0, multiplier->GetOutputPipe(0));
		}
		else
		{
			lowPassFilter->SetInputPipe(0, generator->GetOutputPipe(0));
		}

		auto pipe = pl.CreatePipe();

		lowPassFilter->SetOutputPipe(0, pipe);
		cout << "Low pass filter has been set." << endl;
	}

	std::string filename;
	cout << "Name of file(format : 'name.wav'" << endl;
	cin >> filename;

	StagePtr waveOutStage;
	waveOutStage = pl.AddStage<WaveFileWriter>(WaveFileWriter::Params(filename));

	if (lowPassFilter)
	{
		waveOutStage->SetInputPipe(0, lowPassFilter->GetOutputPipe(0));
	}
	else if (linearEnv && multiplier)
	{
		waveOutStage->SetInputPipe(0, multiplier->GetOutputPipe(0));
	}
	else
	{
		waveOutStage->SetInputPipe(0, generator->GetOutputPipe(0));
	}

	pl.Start();
	cout << "Sound has been generated... exiting." << endl;
	cin.get();
}
#else
int main()
{
	using namespace Synt::Core;
	
	Pipeline pipeline(256);
	
	StagePtr s0 = pipeline.AddStage<Generator, FMSource>(FMSource::Params(62, 2, 2));
	StagePtr s1 = pipeline.AddStage<Filter, LowPassFilter>(LowPassFilter::Params(150.0f));
	//StagePtr s2 = pipeline.AddStage<LinearEnvelope>(LinearEnvelope::Params(0.2f, 0.4f, 0.8f));
	StagePtr s3 = pipeline.AddStage<OutputStreamer>(OutputStreamer::Params());
	//StagePtr s4 = pipeline.AddStage<Multiplier>(Multiplier::Params());
	
	PipePtr<std::vector<AmplitudeVal>> sb0 = pipeline.CreatePipe();
	PipePtr<std::vector<AmplitudeVal>> sb1 = pipeline.CreatePipe();
	PipePtr<std::vector<AmplitudeVal>> sb2 = pipeline.CreatePipe();
	PipePtr<std::vector<AmplitudeVal>> sb3 = pipeline.CreatePipe();

	//PZ::TODO Throw exceptions when trying to create loops
	s0->SetOutputPipe(0, sb0);
	//s2->SetOutputPipe(0, sb1);
	s1->SetInputPipe(0, sb0);
	s1->SetOutputPipe(0, sb2);
	s3->SetInputPipe(0, sb2);

	pipeline.Start();
	
	return 0;
}
#endif
