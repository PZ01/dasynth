#pragma once
#include "utils/Config.hpp"
#include "cmn/Base.hpp"

#include <condition_variable>
#include <queue>
#include <mutex>

namespace Synt
{
namespace Core
{

/**
*SharedQueue class wraps a standard std::Queue for concurrent access of a generic
*type T.
*
*/
template <class T>
class SharedQueue
{

public:
	/**
	*SharedBuffer constructor.
	*@param bufferSize defines the number of elements this buffer can hold.
	*/
	ENGINE_API SharedQueue(const size_t bufferSize)
		: mBufferSize(bufferSize)
		, mMutex()
		, mCondition()
		{
		}

	/**
	*Blocking call which returns by argument reference the top-most value
	*of the queue.
	*@param data type T argument which will receive the last added value of the queue.
	*/
	ENGINE_API void Pop(T &data)
	{
		std::unique_lock<std::mutex> lock(mMutex);

		while (mQueue.empty())
		{
			mCondition.wait(lock);
		}

		data = mQueue.front();
		mQueue.pop();
	}

	/**
	*Pushes argument of type T into the queue. The method notifies any
	*waiting thread to awake from it's waiting state.
	*@param data type T argument which will be 'pushed' into the queue.
	*/
	ENGINE_API void Push(const T& data)
	{
		std::unique_lock<std::mutex> lock(mMutex);
		mQueue.push(data);
		lock.unlock();

		mCondition.notify_one();
	}

	ENGINE_API size_t GetBufferSize()
	{
		return mBufferSize;
	}

private:
	std::queue<T> mQueue;
	const size_t mBufferSize;
	std::mutex mMutex;
	std::condition_variable mCondition;
};

}
}