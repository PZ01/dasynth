#pragma once

namespace Synt
{
namespace Core
{

// Decides whether symbols are imported from a dll (client app) or exported to
// a dll (MyEngine library). 
#if defined(_MSC_VER)

	#if defined(ENGINE_STATICLIB)
		#define ENGINE_API
	#else
		#if defined(ENGINE_SOURCE)
			// If we are building the DLL, export the symbols tagged like this
			#define ENGINE_API __declspec(dllexport)
		#else
			// If we are consuming the DLL, import the symbols tagged like this
			#define ENGINE_API __declspec(dllimport)
		#endif
	#endif

#elif defined(__GNUC__)

#if defined(ENGINE_STATICLIB)
	#define ENGINE_API
#else
	#if defined(ENGINE_SOURCE)
		#define ENGINE_API __attribute__ ((visibility ("default")))
	#else
		// If you use -fvisibility=hidden in GCC, exception handling and RTTI
		// would break if visibility wasn't set during export _and_ import
		// because GCC would immediately forget all type infos encountered.
		// See http://gcc.gnu.org/wiki/Visibility
		#define ENGINE_API __attribute__ ((visibility ("default")))
	#endif
#endif

#else
	
	#error Unknown compiler, please implement shared library macros

#endif

}
}