#pragma once
#include "utils/Config.hpp"
#include "utils/SharedQueue.hpp"
#include <memory>

namespace Synt
{
namespace Core
{
/**
* The Pipe class represents a 'pipe' in the 'Pipe and Filter' architecture.
* The class uses templates to specify the type of data that is passed trough
* the pipe. The size is specified on construction and cannot be changed in this
* current implementation.
*/
template <class T>
class Pipe
{

private:
	SharedQueue<T> mSharedQueue;

public:
	/**
	* Pipe constructor.
	* @param bufferSize the size of the pipe.
	*/
	ENGINE_API Pipe(size_t bufferSize)
		: mSharedQueue(bufferSize)
		{
		}

	/**
	* SharedBuffer can be called to acquire a pointer
	* to the internal data buffer used by the Pipe.
	*/
	ENGINE_API SharedQueue<T>* SharedBuffer()
	{
		return &mSharedQueue;
	}

};

template<class T> using PipePtr = std::shared_ptr< Pipe<T> >;

}
}