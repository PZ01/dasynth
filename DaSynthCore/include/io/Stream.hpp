#pragma once

#include "utils/Config.hpp"
#include "cmn/SyntBase.hpp"
#include "io/portaudio/portaudio.h"
#include <vector>

/**
*
*/
namespace Synt
{
namespace Core
{

class Stream 
{

public:
	/**
	*/
	ENGINE_API Stream(const int32_t framesPerBuffer);

	ENGINE_API void Initialize(const bool defaultDevice = true);
	ENGINE_API void OpenStream();
	ENGINE_API void StartStream();
	ENGINE_API void CloseStream();
	ENGINE_API void StopStream();
	ENGINE_API void WriteStream(const std::vector<AmplitudeVal> &samples);
	ENGINE_API void Terminate();

private:
	PaStreamParameters m_outputParameters;
	PaStream *m_stream; //TODO make smartptr
	bool m_initialized;
	PaError m_err;
	uint32_t m_framesPerBuffer;
	std::vector<AmplitudeVal> m_internalBuffer;
};

} //end namespace Core
} //end namespace Synt
