#pragma once

#include "cmn/Base.hpp"

/**
* Represents a simple RIFF header for WAVE files.
*
* Implementation is based on following guide :
* http://web.archive.org/web/20140204031104/http://www.sonicspot.com/guide/wavefiles.html
*/
namespace Synt
{
namespace Core
{

class WaveHeader
{

public:
	/**
	*WaveHeader constructor.
	*@params channelCount Number of Channels in Signal
	*/
	WaveHeader(uint16_t channelCount);

	/**
	*Writes final data into proper wave headers before finishing
	*writing operation.
	*@params chunckRiffSize Samples written + header size in bytes.
	*@params chunckDataSize total amount of samples in bytes.
	*/
	void UpdateHeaderForClose(int32_t chunkRiffSize, int32_t chunkDataSize);

private:

	static const int8_t mIdLength = 4;

	struct RiffId 
	{
		int8_t id[mIdLength]; 
	};

	struct RiffChunk
	{
		RiffId riffId;
		int32_t chunckDataSize;
	};

	struct FormatChunck
	{
		int16_t compressionCode;
		int16_t channelCount;
		int32_t sampleRate;
		int32_t averageBps;               // = sampleRate * blockAlignment 
		int16_t blockAlignment;           // = significantBitsPerSample / 8 * NumChannels
		int16_t significantBitsPerSample; // = a multiple of eight
		// Ignoring Extra Format
	};

	// Full Wave file header definition, this order should be respected.
	struct WaveFileHeader
	{
		RiffChunk riffChunk;
		RiffId waveId;
		RiffChunk formatChunck;
		FormatChunck formatData;
		RiffChunk dataChunck;
	};

	struct WaveFileHeader mWaveHeader;
};

} //end namespace Core
} //end namespace Synt
