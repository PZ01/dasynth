#pragma once

#include <fstream>
#include <string>

#include "utils/Config.hpp"
#include "io/WaveHeader.hpp"
#include "cmn/SyntBase.hpp"
#include "cmn/StageDefs.hpp"
#include "cmn/Stage.hpp"

// Forward Declarations
class SharedQueue; //TODO, remove

/**
* Specialized Stage which allows the writing to disk of a wave file.
* 
* This class can write in chunks.
*/
namespace Synt
{
namespace Core
{
class WaveFileWriter : public Stage
{
public:
	typedef struct Params
	{
		/**
		* WaveFileWriter constructor arguments.
		* @param filePath String specifying where the wave file will be written
		* in the filesystem.
		* @param channelCount number of channels used in the signal
		*/
		Params(std::string filePath, uint16_t channelCount = 1)
		: filePath(filePath)
		, channelCount(channelCount)
		{}
		const StageId id = StageId::WAVE16BIT_WRITER;
		std::string filePath;
		uint16_t channelCount;

		Params& operator=(const Params&) = delete;
	};

	/**
	* WaveFileWriter constructor.
	* @param params WaveFileWriter parameters.
	*/
	ENGINE_API WaveFileWriter(const Params &params);

	/**
	* Opens the stream for writing. This method will assert
	* if the path is empty.
	*/
	ENGINE_API void Open(); //TODO throw exception

	/**
	* Flushes and closes the stream to write final segments of wave file.
	*/
	ENGINE_API void Close(); //TODO throw exception

	/**
	* Writes samples to a buffer which will be written on disk.
	* This method will assert if the stream is closed.
	* @param value Amplitude value of the signal at a given time.
	* @return always returns true for now.
	*/
	ENGINE_API bool WriteSample(AmplitudeVal value);

private:
	void Init();
	bool Work();
	void Finish();

private:
	std::ofstream mOutStream;
	uint32_t mSamplesWritten;
	std::string mFilePath;
	WaveHeader mWaveHeader;
};

} //end namespace Core
} //end namespace Synt
