#pragma once

#include "utils/Config.hpp"
#include "cmn/StageDefs.hpp"
#include "cmn/Stage.hpp"
#include "io/Stream.hpp"

namespace Synt
{
namespace Core
{
/**
*
*/
class OutputStreamer : public Stage
{

public:
	typedef struct Params
	{
		Params(int32_t framesPerBuffer = 1024)
		: framesPerBuffer(framesPerBuffer)
		{}
		const StageId id = StageId::DAC_STAGE;
		uint32_t framesPerBuffer;
		Params& operator=(const Params&) = delete;
	};

public:
	ENGINE_API OutputStreamer(const Params &params);

private:
	void Init();
	bool Work();
	void Finish();

private:
	Stream m_stream;
};

}
}