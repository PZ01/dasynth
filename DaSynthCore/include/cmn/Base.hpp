#pragma once

// Uncomment this line to disable asserts and debug messages
//#define NDEBUG

// Include assertions
#include <assert.h>

// Include fixed-width primitive ints
#include <stdint.h>

#ifndef NDEBUG
	// printf
	#include <stdio.h>
#endif

#ifdef NDEBUG
	#define PRINT_DBG(msg) ((void) 0)
#else
	#define PRINT_DBG(msg) (printf(msg))
#endif // NDEBUG
