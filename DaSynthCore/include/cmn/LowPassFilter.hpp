#pragma once
#include "cmn/BiQuadFilter.hpp"

namespace Synt
{
namespace Core
{
/**
* The LowPassFilter class is a concrete implementation of a low-pass filter
* which calculates the appropriate coefficients.
*/
class LowPassFilter : public BiQuadFilter
{
public:
	typedef struct Params
	{
		/**
		* Parameter constructor.
		* @param frequencyCutOff Frequencies in the signal above the frequency cutoff will be eliminated.
		* @param amplitudeGain Alters the amplitude of the signal
		*/
		Params(FrequencyVal frequencyCutOff, AmplitudeVal amplitudeGain = 1.0f)
		: freqCutOff(frequencyCutOff)
		, ampGain(amplitudeGain)
		{}
		const StageId id = StageId::LOWPASS_FILTER;
		FrequencyVal freqCutOff;
		AmplitudeVal ampGain;
		Params& operator=(const Params &) { return *this; }
	};

public:
	/**
	* LowPassFilter constructor.
	* @see BiQuadFilter constructor.
	*/
	LowPassFilter(const LowPassFilter::Params &params)
		: BiQuadFilter(params.freqCutOff, params.ampGain)
	{
		CalculateCoefficients();
	}

private:
	/**
	* CalculateCoefficients computes the coefficients used in a low-pass
	* filter.
	*/
	void CalculateCoefficients()
	{
		double w0 = SyntConfig::GetInstance().frequencyRad * mFreqCutOff;
		double cos_w0 = cos(w0);
		double alph = sin(w0) / (2 * mFrequencyQ);

		// LPF-specific
		double b0 = (1 - cos_w0) / 2;
		double b1 = 1 - cos_w0;
		double b2 = b0;
		double a0 = 1 + alph;
		double a1 = -2 * cos_w0;
		double a2 = 1 - alph;

		mb0a0 = static_cast<float>(b0 / a0);
		mb1a0 = static_cast<float>(b1 / a0);
		mb2a0 = static_cast<float>(b2 / a0);
		ma1a0 = static_cast<float>(a1 / a0);
		ma2a0 = static_cast<float>(a2 / a0);
	}
};

} //end namespace Core
} //end namespace Synt