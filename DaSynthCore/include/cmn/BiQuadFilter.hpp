#pragma once
#include "utils/Config.hpp"
#include "cmn/SyntBase.hpp"
#include "cmn/StageDefs.hpp"

namespace Synt
{
namespace Core
{

/**
*Interface representing a BiQuadFilter.
*
*The filter described here is a Direct Form 1, second-order BiQuadFilter.
*It uses coefficients calculated in concrete classes to do the filtering. 
*
*The equations and coefficients used in this code are available at :
*http://www.musicdsp.org/files/Audio-EQ-Cookbook.txt
*/
class BiQuadFilter
{
public:
	/**
	* Filter uses a Direct Form I with a history of two samples.
	* The Filter method can be called after CalculateCoefficients()
	* has been called to set the proper coefficients.
	* @see CalculateCoefficients()
	* @param x the current input amplitude used in the filtering(i.e x[n])
	* @return The filtered amplitude(i.e y[n])
	*/
	AmplitudeVal Filter(AmplitudeVal x)
	{
		// Direct Form I
		//y[n] = (b0/a0)*x[n] + (b1/a0)*x[n-1] + (b2/a0)*x[n-2]
		// -(a1 / a0)*y[n - 1] - (a2 / a0)*y[n - 2]

		AmplitudeVal y = (mb0a0 * x) + (mb1a0 * mDelayIn1) + (mb2a0 * mDelayIn2)
			- (ma1a0 * mDelayOut1) - (ma2a0 * mDelayOut2);

		mDelayIn2 = mDelayIn1;
		mDelayIn1 = x;
		mDelayOut2 = mDelayOut1;
		mDelayOut1 = y;

		return y * mGain;
	}

	/**
	* Frequency cut off setter for dynamic changes.
	* @param cutOff the new frequency that will override the old one.
	*/
	void SetFrequencyCutOff(FrequencyVal cutOff)
	{
		mFreqCutOff = cutOff;
	}

	/**
	* Quantization frequency.
	* @param freqQ the new quantization that will override the old one.
	*/
	void SetFrequencyQ(FrequencyVal freqQ)
	{
		mFrequencyQ = freqQ;
	}

protected:
	/**
	* BiQuadFilter constructor.
	* @param cutOff a frequency representing the frequency cut off at which the filter operates on.
	* @param gain the gain that is multiplied against the filtered value.
	*/
	BiQuadFilter(FrequencyVal cutOff, AmplitudeVal gain = 1.0f)
		: mFreqCutOff(cutOff)
		, mGain(gain)
		, mFrequencyQ(1.0f)
		, ma1a0(0.0f)
		, ma2a0(0.0f)
		, mb0a0(0.0f)
		, mb1a0(0.0f)
		, mb2a0(0.0f)
		, mDelayIn1(0.0f)
		, mDelayIn2(0.0f)
		, mDelayOut1(0.0f)
		, mDelayOut2(0.0f)
	{
	}

	/**
	* CalculateCoefficients must be implemented by a concrete class.
	* This method has to be called before a call to Filter is made.
	* This method typically calculates and sets each coefficient ratio defined
	* in this interface.
	* @see Filter(AmplitudeVal x)
	*/
	virtual void CalculateCoefficients() = 0;

protected:
	AmplitudeVal mGain;
	AmplitudeVal mb0a0;
	AmplitudeVal mb1a0;
	AmplitudeVal mb2a0;
	AmplitudeVal ma1a0;
	AmplitudeVal ma2a0;
	AmplitudeVal mDelayIn1;
	AmplitudeVal mDelayIn2;
	AmplitudeVal mDelayOut1;
	AmplitudeVal mDelayOut2;
	FrequencyVal mFreqCutOff;
	FrequencyVal mFrequencyQ;
};

} //end namespace Core
} //end namespace Synt