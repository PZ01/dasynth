#pragma once
#include "utils/Config.hpp"
#include "cmn/SyntBase.hpp"
#include "cmn/StageDefs.hpp"
#include <vector>

namespace Synt
{
namespace Core
{

/**
* This generator source implements an AM(Amplitude Modulator) generator.
* More details to come.
*/
class AMSource
{
public:
	typedef struct Params
	{
		/**
		* FM parameters
		* @param modulatorMultiplier Multiple of the carrier. Determines the
		* modulator frequency.
		* @param sidebandAmplitude controls the amplitude of the modulator,
		* a value between 0 and 1.0f 
		*/
		Params(uint16_t noteId, uint16_t modulatorMultiplier, AmplitudeVal sidebandAmplitude)
		: noteId(noteId)
		, modMult(modulatorMultiplier)
		, sidebandAmplitude(sidebandAmplitude)
		{
			assert(sidebandAmplitude > 0.0f && sidebandAmplitude <= 1.0f);
		}
		const StageId id = StageId::AM_SOURCE;
		uint16_t noteId;
		uint16_t modMult;
		AmplitudeVal sidebandAmplitude;
		Params& operator=(const Params &) { return *this; }
	};

public:
	/**
	*AM constructor
	*@param params SinOscillator parameters
	*/
	ENGINE_API AMSource(const AMSource::Params &params);

	/**
	* Fills a buffer of samples by a number of samples with a sinusoidal function 
	* between -1.0f and 1.0f
	* @param samplesBuffer Buffer which will be filled with requested samples
	* @param samplesToGenerate Number of samples that will be generated on Fill call
	*/
	ENGINE_API void Fill(std::vector<AmplitudeVal> &samplesBuffer, const long samplesToGenerate);

private:
	FrequencyVal mNoteFreq;
	FrequencyVal mModulatorFreq;
	PhaseAccumulator mModulatorIncrement;
	PhaseAccumulator mModulatorPhase;
	PhaseAccumulator mCarrierIncrement;
	PhaseAccumulator mCarrierPhase;
	AmplitudeVal mModulatorAmplitude;
	AmplitudeVal mVolume; // Same as Carrier Amplitude...
	AmplitudeVal mModulatorScale;
};

} //end namespace Core
} //end namespace Synt