#pragma once

namespace Synt
{
namespace Core
{

enum class StageId
{
	DAC_STAGE,
	SIN_OSCILLATOR,
	FM_SOURCE,
	AM_SOURCE,
	NOISE_GENERATOR,
	WAVE16BIT_WRITER,
	LINEAR_ENVELOPE,
	MULTIPLIER,
	STREAMER,
	LOWPASS_FILTER
};


} //end namespace Core
} //end namespace Synt
