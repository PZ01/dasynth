#pragma once
#include "cmn/SyntBase.hpp"
#include "utils/Config.hpp"
#include "utils/Pipe.hpp"

namespace Synt
{
namespace Core
{
/**
*Represents a typical 'Filter' in the 'Pipe and Filter'
*pattern. A Stage exists in it's own thread and uses a number
*of input and output pipes communication within the pipeline.
*
*The interface provided let's subclasses override a Init, Work
*and Finish methods which are called by the Pipeline trough the Process
*method. This interface also provides facilities to assign pipes.
*/
class Stage
{
public:
	/**
	*Calling this method will execute Init, Work and Finish
	*on callee. The Work method will return once computation is over.
	*/
	ENGINE_API void Process();

	/**
	*@return Number of input pipes used by this stage
	*/
	ENGINE_API const uint16_t InputCount() { return mInputCount; }

	/**
	*@return Number of ouput pipes used by this stage
	*/
	ENGINE_API const uint16_t OutputCount() { return mOutputCount; }

	/**
	*Used to obtain a shared pointer to the output pipe at a given index.
	*@param idx Index at which the output pipe has been set previously.
	*@return shared pointer containing the Pipe at index idx.
	*/
	ENGINE_API PipePtr< std::vector<AmplitudeVal> > GetOutputPipe(const size_t idx); // PZ::Todo should throw exceptions

	/**
	*Used to obtain a shared pointer to the input pipe at a given index.
	*@param idx Index at which the input pipe has been set previously.
	*@return shared pointer containing the Pipe at index idx.
	*/
	ENGINE_API PipePtr< std::vector<AmplitudeVal> > GetInputPipe(const size_t idx);

	/**
	*Used to set a shared pointer containing a pipe to the list of active
	*outputs which the stage holds on to. The pipe is set to index idx.
	*@param idx Index specifying where the pipe will be assigned in the stage
	*output list.
	*/
	ENGINE_API void SetOutputPipe(size_t idx, PipePtr< std::vector<AmplitudeVal> > outputPipe);

	/**
	*Used to set a shared pointer containing a pipe to the list of active
	*inputs which the stage holds on to. The pipe is set to index idx.
	*@param idx Index specifying where the pipe will be assigned in the stage
	*output list.
	*/
	ENGINE_API void SetInputPipe(size_t idx, PipePtr< std::vector<AmplitudeVal> > outputPipe);

protected:
	ENGINE_API Stage(const uint16_t inputCount, const uint16_t outputCount);
	virtual void Init() {}; 
	virtual bool Work() = 0;
	virtual void Finish() {};

protected:
	const uint16_t mInputCount;
	const uint16_t mOutputCount;
	std::vector< PipePtr<std::vector<AmplitudeVal> > > mInputs;
	std::vector< PipePtr<std::vector<AmplitudeVal> > > mOutputs;

private:
	// As of right now stages can't be copied
	Stage& operator=(const Stage&) = delete;
	Stage(const Stage& other) = delete;
};

typedef std::shared_ptr<Stage> StagePtr;

} //end namespace Core
} //end namespace Synt