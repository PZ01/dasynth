#pragma once
#include "utils/Config.hpp"
#include "cmn/Base.hpp"
#include "cmn/Stage.hpp"
#include "cmn/StageRegistry.hpp"
#include <memory>

namespace Synt
{
namespace Core
{
/**
*The Pipeline class represents the whole 'Pipe and Filter'
*architecture.
*
*This class holds the references to the various stages that exist
*in the system and is the interface that allows creation of Stage objects
*as well as Pipe objects. Stages can be connected with the created pipes.
*
*This class exposes methods for starting and stopping the execution flow
*and executes each Stage within it's own thread.
*/
class Pipeline
{

public:
	/**
	*Pipeline constructor.
	*@param sizeOfPipes Specifies the size of the pipes that
	*connect different stages together. As it stands, the size
	*of all pipes is the same and cannot be changed.
	*/
	ENGINE_API Pipeline(const int32_t sizeOfPipes); 

	/**
	*Create and adds a stage to the Pipeline.
	*@param params The constructor params of the stage that will be created and added.
	*@return A shared pointer representing the specialized stage that was created and added.
	*/
	template<typename T>
	std::shared_ptr<T> AddStage(typename T::Params params)
	{
		std::shared_ptr<T> stage = mStageRegistry.CreateStage<T>(params);
		assert(stage);
		mStages.push_back(stage);

		return stage;
	}

	/**
	*Create and adds a stage to the Pipeline. This method is used when
	*a stage is defined as having a source.
	*@param params The constructor params of the source that defines the stage.
	*@return A shared pointer representing the specialized stage
	*and it's source that was created and added.
	*@see AddStage(typename T::Params params)
	*/
	template<template<class> class H, class Src>
	std::shared_ptr< H<Src> > AddStage(typename Src::Params params)
	{
		std::shared_ptr< H<Src> > stage = mStageRegistry.CreateStage<H, Src>(params);
		assert(stage);
		mStages.push_back(stage);

		return stage;
	}

	/**
	*Creates and returns a pipe with a size defined by the size
	*specified in the Pipeline constructor.
	*@return A shared pointer representing a pipe which holds float values.
	*/
	ENGINE_API PipePtr< std::vector<AmplitudeVal> > CreatePipe();

	/**
	*Starts the execution of the Pipeline. This method will
	*currently assert if no stages exist in the Pipeline.
	*/
	ENGINE_API void Start();

private:
	StageRegistry mStageRegistry;
	const int32_t mSizeOfPipes; //TODO Should be a multiple of 2
	std::vector< std::shared_ptr<Stage> > mStages;

	Pipeline& operator=(const Pipeline&) = delete;
	Pipeline(const Pipeline& other) = delete;
};

}
}