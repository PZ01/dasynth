#pragma once
#include "utils/Config.hpp"
#include "cmn/SyntBase.hpp"
#include "cmn/StageDefs.hpp"
#include <vector>

namespace Synt
{
namespace Core
{

/**
* This generator source provides a periodic sin signal.
*/
class SinOscillatorSource
{
public:
	typedef struct Params
	{
		/**
		*SinOscillator parameters
		*@param noteId Note with an octave which is converted to a given frequency.
		*/
		Params(int16_t noteId)
		: noteId(noteId)
		{}
		const StageId id = StageId::SIN_OSCILLATOR;
		int16_t noteId;

		Params& operator=(const Params&) = delete;
	};

public:
	/**
	*SinOscillator constructor
	*@param params SinOscillator parameters
	*/
	ENGINE_API SinOscillatorSource(const SinOscillatorSource::Params &params);

	/**
	* Fills a buffer of samples by a number of samples with a sinusoidal function 
	* between -1.0f and 1.0f
	* @param samplesBuffer Buffer which will be filled with requested samples
	* @param samplesToGenerate Number of samples that will be generated on Fill call
	*/
	ENGINE_API void Fill(std::vector<AmplitudeVal> &samplesBuffer, const long samplesToGenerate);

private:
	FrequencyVal mGeneratedFrequency;
	FrequencyVal mVolume;
	PhaseAccumulator mPhase;
	PhaseAccumulator mPhaseIncrement;
};

} //end namespace Core
} //end namespace Synt