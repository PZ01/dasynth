#pragma once
#include "utils/Config.hpp"
#include "cmn/SyntBase.hpp"
#include "cmn/StageDefs.hpp"
#include "cmn/Stage.hpp"

namespace Synt
{
namespace Core
{
/**
* A simple fixed-duration linear envelope.
* This LinearEnvelope stage generates a linear envelope with an attack
* and a release portion specified in time. The sustain portion is the remaining
* time.
*/
class LinearEnvelope: public Stage
{

public:
	/**
	* Struct representing the arguments and the type of the stage.
	* This struct is passed to the constructor of this class to initialize
	* it's members.
	* @see LinearEnvelope(const Params &params)
	*/
	typedef struct Params
	{
		/**
		* Parameter constructor.
		* @param attack is the attack portion of the envelope, specified in seconds.
		* @param decay is the decay(release) portion of the envelope, specified in seconds.
		* @param peakAmplitude is the envelopes maximum amplitude, default is 1.
		*/
		Params(SecondsVal attack, SecondsVal decay, AmplitudeVal peakAmplitude = 1.0f)
		: atk(attack)
		, dcy(decay)
		, peakAmp(peakAmplitude)
		{}
		const StageId id = StageId::LINEAR_ENVELOPE;
		SecondsVal atk;
		SecondsVal dcy;
		AmplitudeVal peakAmp;

		Params& operator=(const Params&) = delete;
	};

	/**
	* LinearEnvelope constructor.
	* @param params a parameter struct of the linear envelope.
	*/
	ENGINE_API LinearEnvelope(const Params &params);

private:
	void Init();
	bool Work();

	/**
	* Method can be used to seek to a specific time in the envelope
	* generation.
	* @param currPhase the phase at which we want to seek to. A value of
	* zero represents a reset.
	*/
	void Seek(AmplitudeVal currPhase = 0.0f);

	LinearEnvelope& operator=(const LinearEnvelope&) = delete;
	LinearEnvelope(const LinearEnvelope& other) = delete;

private:
	unsigned long mTotalSamples;
	AmplitudeVal mEnvelopeIncrement;
	AmplitudeVal mPeakAmplitude;
	AmplitudeVal mVolume;
	uint32_t mAttackSamples;
	uint32_t mSustainSamples;
	uint32_t mDecaySamples;
	uint32_t mGeneratedSamples;
};

} //end namespace Core
} //end namespace Synt