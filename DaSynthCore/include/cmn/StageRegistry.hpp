#pragma once
#include "utils/Config.hpp"
#include "cmn/Base.hpp"
#include "cmn/StageDefs.hpp" //PZ::TODO may not need these includes anymore
#include "cmn/LinearEnvelope.hpp"
#include "cmn/Multiplier.hpp"
#include "io/WaveFileWriter.hpp"

#include <vector>

namespace Synt
{
namespace Core
{
/**
*This class will eventually be responsible for loading and unloading
*stages dynamically. It currently acts as a Factory class for Stages.
*/
//PZ::TODO Might want to make it private and a friend of pipeline
class StageRegistry 
{
	
public:
	template<typename T>
	std::shared_ptr<T> CreateStage(const typename T::Params params)
	{
		return std::make_shared<T>(params);
	}

	template<template<class> class H, class Src>
	std::shared_ptr< H<Src> > CreateStage(const typename Src::Params params)
	{
		return std::make_shared< H<Src> >(params);
	}

private:
	std::vector<std::string> mLoadedStages;
};

}
}