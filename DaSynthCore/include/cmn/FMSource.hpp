#pragma once
#include "utils/Config.hpp"
#include "cmn/SyntBase.hpp"
#include "cmn/StageDefs.hpp"
#include <vector>

namespace Synt
{
namespace Core
{

/**
* This generator source implements an FM(Frequency Modulator) generator.
*/
class FMSource
{
public:
	typedef struct Params
	{
		/**
		* FM parameters
		* @param modulatorMultiplier Multiple of the carrier. Determines the
		* modulator frequency.
		* @param indexOfModulation is a measure of the peak frequency
		* deviation and is defined as AmpModulation/FreqModulation
		*/
		Params(uint16_t noteId, uint16_t modulatorMultiplier, uint16_t indexOfModulation)
		: noteId(noteId)
		, modMult(modulatorMultiplier)
		, beta(indexOfModulation)
		{}
		const StageId id = StageId::FM_SOURCE;
		uint16_t noteId;
		uint16_t modMult;
		uint16_t beta;
		Params& operator=(const Params &) { return *this; }
	};

public:
	/**
	*FM constructor
	*@param params SinOscillator parameters
	*/
	ENGINE_API FMSource(const FMSource::Params &params);

	/**
	* Fills a buffer of samples by a number of samples with a sinusoidal function 
	* between -1.0f and 1.0f
	* @param samplesBuffer Buffer which will be filled with requested samples
	* @param samplesToGenerate Number of samples that will be generated on Fill call
	*/
	ENGINE_API void Fill(std::vector<AmplitudeVal> &samplesBuffer, const long samplesToGenerate);

private:
	FrequencyVal mNoteFreq;
	FrequencyVal mModulatorFreq;
	PhaseAccumulator mModulatorIncrement;
	PhaseAccumulator mModulatorPhase;
	PhaseAccumulator mCarrierIncrement;
	PhaseAccumulator mCarrierPhase;
	AmplitudeVal mModulatorAmplitude;
	AmplitudeVal mVolume; // Equivalent to Carrier Amplitude...
};

} //end namespace Core
} //end namespace Synt