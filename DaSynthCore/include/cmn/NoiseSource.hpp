#pragma once
#include "utils/Config.hpp"
#include "cmn/SyntBase.hpp"
#include "cmn/StageDefs.hpp"
#include <vector>

namespace Synt
{
namespace Core
{

/**
* The NoiseSource class is used as the source of a generator stage.
*/
class NoiseSource
{
public:
	typedef struct Params
	{
		const StageId id = StageId::NOISE_GENERATOR;
		Params& operator=(const Params&) = delete;
	};

public:
	/**
	* NoiseSource constructor.
	* @param params NoiseSource parameters
	*/
	ENGINE_API NoiseSource(const NoiseSource::Params &params);

	/**
	* Fills a buffer of samples by a number of samples with random values
	* between -1.0f and 1.0f
	* @param samplesBuffer Buffer which will be filled with requested samples
	* @param samplesToGenerate Number of samples that will be generated on Fill call
	*/
	ENGINE_API void Fill(std::vector<AmplitudeVal> &samplesBuffer, const long samplesToGenerate);
};

} //end namespace Core
} //end namespace Synt