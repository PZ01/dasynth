#pragma once
#include "utils/Config.hpp"
#include "cmn/SyntBase.hpp"
#include "cmn/StageDefs.hpp"
#include "cmn/Stage.hpp"

namespace Synt
{
namespace Core
{

/**
*A specialized stage used for signal generation.
*
*The generator is a stage with no input pipe and a single output pipe.
*It is generic until a Src class is provided that provides the signals
*algorithm to fill the output pipe.
*
*/
template <class Src>
class Generator : public Stage
{

public:
	Generator(const typename Src::Params &params);

private:
	/**
	* Overridden Work method from Stage base class that
	* fills an output pipe with a signal specified by Src.
	* @see bool Stage::Work();
	*/
	bool Work();

private:
	Src mGenerationSrc;
	FrequencyVal mGeneratedFrequency;
	long mTotalSamples;
	long mGeneratedSamples;
	PhaseAccumulator mPhase;
	PhaseAccumulator mPhaseIncrement;
};

//////////////////////////////////////////////////////////////////////////
//Template Implementation
//////////////////////////////////////////////////////////////////////////

/**
* Generator constructor. Requires a Src template
* along with the parameters to construct the Src object.
* @param params constructor parameters of Src template provided
*/
template <typename Src>
Generator<Src>::Generator(const typename Src::Params &params)
	: Stage(0, 1)
	, mGenerationSrc(params)
	, mGeneratedSamples(0)
{
	FrequencyVal dur = SyntConfig::GetInstance().GetDuration();
	if (dur == NO_DURATION)
	{
		mTotalSamples = NO_DURATION;
	}
	else
	{
		mTotalSamples = (long)((SyntConfig::GetInstance().sampleRate * dur) + 0.5);
	}

}

template <class Src>
bool Generator<Src>::Work()
{
	// Currently if this stage isn't hooked up it will assert...
	assert(mOutputs[0] != nullptr);
	auto outputBuf = mOutputs[0]->SharedBuffer();

	std::vector<AmplitudeVal> localSamples;//TODO reserve...
	bool keepGenerating = true;
	long samplesToGenerate = outputBuf->GetBufferSize();

	if (mTotalSamples != NO_DURATION)
	{
		if (mGeneratedSamples + samplesToGenerate > mTotalSamples)
		{
			samplesToGenerate = mTotalSamples - mGeneratedSamples;
		}

		if (samplesToGenerate == 0)
		{
			assert(localSamples.size() == 0);
			localSamples.push_back(FLT_MAX);
			keepGenerating = false;
		}
	}
	
	mGenerationSrc.Fill(localSamples, samplesToGenerate);
	mGeneratedSamples += samplesToGenerate;

	outputBuf->Push(localSamples);

	return keepGenerating;
}

} //end namespace Core
} //end namespace Synt