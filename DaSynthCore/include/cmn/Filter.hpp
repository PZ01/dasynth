#pragma once
#include "utils/Config.hpp"
#include "cmn/SyntBase.hpp"
#include "cmn/StageDefs.hpp"
#include "cmn/Stage.hpp"
#include "cmn/LowPassFilter.hpp"

namespace Synt
{
namespace Core
{

/**
* 
*/
template <class FilterT>
class Filter : public Stage
{
public:
	/**
	* LowPassStage constructor.
	* @param params Params struct of the LowPassStage
	*/
	Filter(const typename FilterT::Params &params);

private:
	bool Work();

private:
	FilterT mFilter;
};

//////////////////////////////////////////////////////////////////////////
//Template Implementation
//////////////////////////////////////////////////////////////////////////
template <typename FilterT>
Filter<FilterT>::Filter(const typename FilterT::Params &params)
	: Stage(1, 1)
	, mFilter(params)
{

}

template <typename FilterT>
bool Filter<FilterT>::Work()
{
	// There is no filtering to be done if there is no input.
	if (mInputs[0] == nullptr)
	{
		return false;
	}

	auto inputBuf = mInputs[0]->SharedBuffer();
	auto outputBuf = mOutputs[0]->SharedBuffer();

	std::vector<float> inputSamples;
	inputBuf->Pop(inputSamples);

	assert(inputSamples.size() > 0);

	bool keepGenerating = true;
	if (inputSamples.at(0) == FLT_MAX)
	{
		inputSamples[0] = FLT_MAX;
		keepGenerating = false;
	}
	else
	{
		uint32_t idx = 0;
		while (idx < inputSamples.size())
		{
			inputSamples[idx] = mFilter.Filter(inputSamples[idx]);
			++idx;
		}
	}

	// We only push forward if an output exists.
	if (mOutputs[0] != nullptr)
	{
		outputBuf->Push(inputSamples);
	}	

	return keepGenerating;
}

} //end namespace Core
} //end namespace Synt