#pragma once

#include "utils/Config.hpp"
#include "cmn/Base.hpp"
#include <math.h>

namespace Synt
{
namespace Core
{
// Sample represented as a 16-bit short
typedef int16_t SampleVal;

// Type for an amplitude value
typedef float AmplitudeVal;

// Type for a frequency value
typedef float FrequencyVal;

// Type for a time value in seconds
typedef float SecondsVal;

//PZ::Check type
typedef float PhaseAccumulator;

// Generic Parameter alias
typedef float GenericParam;

const float PI = 3.14159265358979323846f;
const float TWO_PI = 2 * PI;
const uint16_t FREQUENCY_RANGE = 108; //C0 to B8

const int16_t NO_DURATION = -1;

/**
*
*/
class SyntConfig
{
public:
	const FrequencyVal sampleRate;
	const FrequencyVal nyquistLimit;
	const PhaseAccumulator frequencyRad;
	AmplitudeVal sampleScale;
	FrequencyVal duration;

	/**
	*Acquire the global SyntConfig object instance.
	*@return SyntConfig object reference.
	*/
	ENGINE_API static SyntConfig& GetInstance()
	{
		static SyntConfig instance;

		return instance;
	}

	/**
	*Converts a pitch to a given frequency.
	*return Frequency of the corresponding pitch.
	*/
	ENGINE_API FrequencyVal GetFrequency(int32_t pitch)
	{
		assert(pitch >= 0 && pitch < FREQUENCY_RANGE);
		return pitchFrequencies[pitch];
	}

	/**
	*@return Fixed duration used by the synthesizer during
	*execution.
	*/
	ENGINE_API FrequencyVal GetDuration()
	{
		return duration;
	}

	/**
	*Not used in the moment.
	*/
	ENGINE_API void SetDuration(FrequencyVal pDuration)
	{
		duration = pDuration;
	}

	/**
	*Convert an amplitude value in the range -1.0f to 1.0f
	*to a 16-bit short representation(-32768 to +32768)
	*/
	ENGINE_API SampleVal ConvertAmplitude(AmplitudeVal value)
	{
		AmplitudeVal sampleScale = SyntConfig::GetInstance().sampleScale;
		SampleVal sampleVal = static_cast<int16_t>(value * sampleScale);

		return sampleVal; //PZ::Todo try returning a reference
	}

private:
	FrequencyVal pitchFrequencies[FREQUENCY_RANGE];

private:
	SyntConfig()
		: sampleRate(44100) // 44100 Hz is the industry standard for CD's
		, nyquistLimit(sampleRate * 0.5f) // PZ::Explain
		, frequencyRad(TWO_PI / sampleRate) // PZ::Explain
		, sampleScale(pow(2.0f, 15) - 1) // Range for PCM format, 0 is silence hence -1
		, duration(NO_DURATION) 
	{
		float aMinus1 = 13.75f; // Standard Frequency of A-1(Subsubcontra)
		float frq = aMinus1 * pow(2.0f, (3.0f / 12.0f)); // Move up by three half-steps
		float octaveStep = pow(2.0f, (1.0f / 12.0f)); // Law of exponents

		for (uint16_t i = 0; i < FREQUENCY_RANGE; ++i)
		{
			pitchFrequencies[i] = frq;
			frq = frq * octaveStep;
		}
	}

	SyntConfig(SyntConfig const&);
	void operator = (SyntConfig const&);
};

} //end namespace Core
} //end namespace Synt