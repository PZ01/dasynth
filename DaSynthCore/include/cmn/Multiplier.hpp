#pragma once
#include "utils/Config.hpp"
#include "cmn/SyntBase.hpp"
#include "cmn/StageDefs.hpp"
#include "cmn/Stage.hpp"

namespace Synt
{
namespace Core
{

/**
*A specialized stage used to multiply two signals together.
*
*The Multiplier is a specialized stage used to multiply two signals
*together in the form Signal1 * Signal2 => OutputSignal1. The multiplier
*uses two input pipes and an output pipe with the multiplied signal.
*As it stands, no validation for overflow or folding is done.
*/
class Multiplier : public Stage
{

public:
	typedef struct Params
	{
		const StageId id = StageId::MULTIPLIER;
		Params& operator=(const Params&) = delete;
	};

	/**
	* Multiplier class constructor.
	* @param params Params struct of the Multiplier 
	*/
	ENGINE_API Multiplier(Params params);

private:
	bool Work();

};

} //end namespace Core
} //end namespace Synt