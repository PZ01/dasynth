#include "cmn/Stage.hpp"
#include <stdexcept>

namespace Synt
{
namespace Core
{

Stage::Stage(const uint16_t inputCount, const uint16_t outputCount)
	: mInputCount(inputCount)
	, mOutputCount(outputCount)
	, mInputs(mInputCount)
	, mOutputs(mOutputCount)
{
	
}

PipePtr< std::vector<AmplitudeVal> > Stage::GetOutputPipe(const size_t idx)
{
	if (idx < 0 || idx > mOutputCount)
		throw std::out_of_range("Provided index is out of range.");
	if (mInputCount == 0)
		throw std::logic_error("No output slot exist on this stage.");

	return mOutputs[idx];
}

PipePtr< std::vector<AmplitudeVal> > Stage::GetInputPipe(const size_t idx)
{
	if (idx < 0 || idx > mOutputCount)
		throw std::out_of_range("Provided index is out of range.");
	if (mInputCount == 0)
		throw std::logic_error("No input slot exist on this stage.");

	return mInputs[idx];
}

void Stage::SetOutputPipe(size_t idx, PipePtr< std::vector<AmplitudeVal> > outputPipe)
{
	if (idx < 0 || idx > mOutputCount)
		throw std::out_of_range("Provided index is out of range.");
	if (mOutputCount == 0)
		throw std::logic_error("No output slot exist on this stage.");

	mOutputs[idx] = outputPipe;
}

void Stage::SetInputPipe(size_t idx, PipePtr< std::vector<AmplitudeVal> > inputPipe)
{
	if (idx < 0 || idx > mOutputCount)
		throw std::out_of_range("Provided index is out of range.");
	if (mInputCount == 0)
		throw std::logic_error("No input slot exist on this stage.");

	mInputs[idx] = inputPipe;
}

void Stage::Process()
{
	Init();
	while(Work());
	Finish();
}

} //end namespace Core
} //end namespace Synt
