#include "cmn/NoiseSource.hpp"

#include <cstdlib>
#include <ctime>

namespace Synt
{
namespace Core
{

NoiseSource::NoiseSource(const NoiseSource::Params &)
{
}

void NoiseSource::Fill(std::vector<AmplitudeVal> &samplesBuf, const long samplesToGenerate)
{
	for (long samples = 0; samples < samplesToGenerate; samples++)
	{
		float r = static_cast<float>((rand() - RAND_MAX/2)) / (RAND_MAX/2);
		samplesBuf.push_back(r);
	}
}

} //end namespace Core
} //end namespace Synt
