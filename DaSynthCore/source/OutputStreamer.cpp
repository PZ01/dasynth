#include "io/OutputStreamer.hpp"

namespace Synt
{
namespace Core
{

OutputStreamer::OutputStreamer(const Params &params)
	: Stage(1,0)
	, m_stream(params.framesPerBuffer)
{
	
}

void OutputStreamer::Init()
{
	m_stream.Initialize();
	m_stream.OpenStream();
	m_stream.StartStream();
}

bool OutputStreamer::Work()
{
	assert(mInputs[0] != nullptr);
	auto inputBuf = mInputs[0]->SharedBuffer();
	
	std::vector<float> localSamples;
	inputBuf->Pop(localSamples);
	assert(localSamples.size() > 0);

	if (localSamples.at(0) == FLT_MAX)
	{
		return false;
	}

	m_stream.WriteStream(localSamples);

	return true;
}

void OutputStreamer::Finish()
{
	m_stream.StopStream();
	m_stream.CloseStream();
	m_stream.Terminate();
}

} //end namespace Core
} //end namespace Synt
