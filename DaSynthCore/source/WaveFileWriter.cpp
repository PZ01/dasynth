#include "io/WaveFileWriter.hpp"
#include "utils/SharedQueue.hpp"
#include <vector>
#include <iostream>

namespace Synt
{
namespace Core
{

WaveFileWriter::WaveFileWriter(const WaveFileWriter::Params &params)
		: Stage(1, 0)
		, mSamplesWritten(0)
		, mFilePath(params.filePath)
		, mWaveHeader(params.channelCount)
{
}

void WaveFileWriter::Open()
{
	assert(!mFilePath.empty());
	mOutStream.open(mFilePath.c_str(), std::ofstream::binary);

	mOutStream.write(reinterpret_cast<char *>(&mWaveHeader), sizeof(mWaveHeader));
}

void WaveFileWriter::Close()
{
	assert(mOutStream.is_open());

	// Updating header before closing file
	uint32_t byteTotal = mSamplesWritten * sizeof(SampleVal);
	mWaveHeader.UpdateHeaderForClose(byteTotal + sizeof(mWaveHeader) - 8, byteTotal);

	mOutStream.seekp(0);
	mOutStream.write(reinterpret_cast<char *>(&mWaveHeader), sizeof(mWaveHeader));
}

bool WaveFileWriter::WriteSample(AmplitudeVal value)
{
	assert(mOutStream.is_open());
	SampleVal sampleVal = SyntConfig::GetInstance().ConvertAmplitude(value);

	// 2 * count because char = 1 byte and our AmplitudeVal(for now) is 2 bytes, we
	// want to write the whole float and not just the first 8 bits.
	mOutStream.write(reinterpret_cast<char *>(&sampleVal), 2);

	mSamplesWritten++;

	return true;
}

void WaveFileWriter::Init()
{
	Open();
}

bool WaveFileWriter::Work()
{
	assert(mInputs[0] != nullptr);
	auto inputBuf = mInputs[0]->SharedBuffer();
	
	std::vector<float> localSamples;
	inputBuf->Pop(localSamples);

	assert(localSamples.size() > 0);

	if (localSamples.at(0) == FLT_MAX)
		return false;

	for (auto it = localSamples.begin(); it != localSamples.end(); ++it) 
	{
		WriteSample(*it);
	}
	
	return true;
}
void WaveFileWriter::Finish()
{
	Close();
}

} //end namespace Core
} //end namespace Synt
