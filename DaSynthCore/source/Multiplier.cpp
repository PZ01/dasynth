#include "cmn/Multiplier.hpp"

namespace Synt
{
namespace Core
{

Multiplier::Multiplier(Multiplier::Params)
	: Stage(2, 1)
{

}

bool Multiplier::Work()
{
	assert(mInputs[0] != nullptr && mInputs[1] != nullptr && mOutputs[0] != nullptr);

	auto signalInputBuf = mInputs[0]->SharedBuffer();
	auto signal2InputBuf = mInputs[1]->SharedBuffer();
	auto outputBuf = mOutputs[0]->SharedBuffer();

	bool keepGenerating = true;

	std::vector<float> localSignal;
	std::vector<float> localAmplitude;
	signalInputBuf->Pop(localSignal);
	signal2InputBuf->Pop(localAmplitude);

	assert(localSignal.size() > 0);
	assert(localAmplitude.size() > 0);
	assert(localSignal.size() == localAmplitude.size());

	if (localSignal.at(0) == FLT_MAX && localAmplitude.at(0) == FLT_MAX)
	{
		localSignal[0] = FLT_MAX;
		keepGenerating = false;
	}
	else
	{
		uint32_t idx = 0;
		while (idx < localSignal.size())
		{
			localSignal[idx] = localSignal[idx] * localAmplitude[idx];
			++idx;
		}
	}

	outputBuf->Push(localSignal);

	return keepGenerating;
}

} //end namespace Core
} //end namespace Synt
