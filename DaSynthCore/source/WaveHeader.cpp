#include "io/WaveHeader.hpp"
#include "cmn/SyntBase.hpp"

namespace Synt
{
namespace Core
{

WaveHeader::WaveHeader(uint16_t channelCount) 
	: mWaveHeader()
{
	mWaveHeader.riffChunk.riffId.id[0] = 'R';
	mWaveHeader.riffChunk.riffId.id[1] = 'I';
	mWaveHeader.riffChunk.riffId.id[2] = 'F';
	mWaveHeader.riffChunk.riffId.id[3] = 'F';
	mWaveHeader.riffChunk.chunckDataSize = 0;
	mWaveHeader.waveId.id[0] = 'W';
	mWaveHeader.waveId.id[1] = 'A';
	mWaveHeader.waveId.id[2] = 'V';
	mWaveHeader.waveId.id[3] = 'E';
	mWaveHeader.formatChunck.riffId.id[0] = 'f';
	mWaveHeader.formatChunck.riffId.id[1] = 'm';
	mWaveHeader.formatChunck.riffId.id[2] = 't';
	mWaveHeader.formatChunck.riffId.id[3] = ' ';
	mWaveHeader.formatChunck.chunckDataSize = sizeof(FormatChunck);
	mWaveHeader.formatData.compressionCode = 1;
	mWaveHeader.formatData.channelCount = channelCount;
	mWaveHeader.formatData.significantBitsPerSample = sizeof(SampleVal) * 8; // bits
	mWaveHeader.formatData.blockAlignment = mWaveHeader.formatData.significantBitsPerSample / 8 * channelCount; // bytes
	mWaveHeader.formatData.sampleRate = static_cast<uint32_t>(SyntConfig::GetInstance().sampleRate);
	mWaveHeader.formatData.averageBps = mWaveHeader.formatData.sampleRate * mWaveHeader.formatData.blockAlignment;
	mWaveHeader.dataChunck.riffId.id[0] = 'd';
	mWaveHeader.dataChunck.riffId.id[1] = 'a';
	mWaveHeader.dataChunck.riffId.id[2] = 't';
	mWaveHeader.dataChunck.riffId.id[3] = 'a';
	mWaveHeader.dataChunck.chunckDataSize = 0;
}

void WaveHeader::UpdateHeaderForClose(int32_t chunkRiffSize, int32_t chunkDataSize)
{
	mWaveHeader.riffChunk.chunckDataSize = chunkRiffSize;
	mWaveHeader.dataChunck.chunckDataSize = chunkDataSize;
}

} //end namespace Core
} //end namespace Synt
