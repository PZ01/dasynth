#include "cmn/Pipeline.hpp"
#include "io/WaveFileWriter.hpp"
#include <thread>
#include <string>
#include <iostream>

namespace Synt
{
namespace Core
{

Pipeline::Pipeline(const int32_t sizeOfPipes)
	: mSizeOfPipes(sizeOfPipes)
	, mStageRegistry()
	, mStages()
{
	if (sizeOfPipes == 0)
		throw std::logic_error("Pipeline cannot have 0-length pipe sizes.");
}

PipePtr< std::vector<AmplitudeVal> > Pipeline::CreatePipe()
{
	return std::make_shared< Pipe< std::vector<AmplitudeVal> > >(mSizeOfPipes);
}

void Pipeline::Start()
{
	if (mStages.size() == 0)
		throw std::logic_error("Pipeline cannot start without stages.");

	std::vector<std::thread> threads;

	for (size_t i = 0; i < mStages.size(); ++i)
	{
		threads.push_back(std::thread(&Stage::Process, mStages[i]));
	}
	for (size_t i = 0; i < mStages.size(); ++i)
	{
		threads[i].join();
	}
}

} //end namespace Core
} //end namespace Synt
