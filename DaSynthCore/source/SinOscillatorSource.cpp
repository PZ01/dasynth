#include "cmn/SinOscillatorSource.hpp"
#include <limits>

namespace Synt
{
namespace Core
{

SinOscillatorSource::SinOscillatorSource(const SinOscillatorSource::Params &params)
	: mGeneratedFrequency(SyntConfig::GetInstance().GetFrequency(params.noteId))
	, mPhaseIncrement(mGeneratedFrequency * SyntConfig::GetInstance().frequencyRad)
	, mPhase(0)
	, mVolume(1.0f)
{
}

void SinOscillatorSource::Fill(std::vector<AmplitudeVal> &samplesBuf, const long samplesToGenerate)
{
	for (long samples = 0; samples < samplesToGenerate; samples++)
	{
		samplesBuf.push_back(mVolume * sin(mPhase));

		if ((mPhase += mPhaseIncrement) >= TWO_PI)
			mPhase -= TWO_PI;
	}
}

} //end namespace Core
} //end namespace Synt
