#include "io/Stream.hpp"
#include "cmn/SyntBase.hpp"

namespace Synt
{
namespace Core
{

Stream::Stream(const int32_t framesPerBuffer)
	: m_framesPerBuffer(framesPerBuffer)
	, m_initialized(false)
{
}

void Stream::Initialize(const bool defaultDevice /*= true*/)
{
	m_err = Pa_Initialize();
	assert(m_err == paNoError);

	if (defaultDevice)
	{
		m_outputParameters.device = Pa_GetDefaultOutputDevice();
		assert(m_outputParameters.device != paNoDevice);

		/*Temporary*/
		m_outputParameters.channelCount = 1;
		m_outputParameters.sampleFormat = paFloat32;
		m_outputParameters.suggestedLatency = Pa_GetDeviceInfo(m_outputParameters.device)->defaultLowOutputLatency;
		m_outputParameters.hostApiSpecificStreamInfo = NULL;
	}
	
	m_initialized = true;
}

void Stream::OpenStream()
{
	//TODO throw exception if m_initialized is false
	m_err = Pa_OpenStream(
		&m_stream,
		NULL, /* no input */
		&m_outputParameters,
		SyntConfig::GetInstance().sampleRate,
		m_framesPerBuffer,
		paClipOff,      /* we won't output out of range samples so don't bother clipping them */
		NULL, /* no callback, use blocking API */
		NULL); /* no callback, so no callback userData */

	assert(m_err == paNoError);
}

void Stream::CloseStream()
{
	m_err = Pa_CloseStream(m_stream);
	assert(m_err == paNoError);
}

void Stream::StartStream()
{
	m_err = Pa_StartStream(m_stream);
	assert(m_err == paNoError);
}

void Stream::StopStream()
{
	m_err = Pa_StopStream(m_stream);
	assert(m_err == paNoError);
}

void Stream::WriteStream(const std::vector<AmplitudeVal> &samples)
{
	//TODO : Throw exception if internal buffer is smaller than buffer frame
	//TODO : Would be better to memcpy all of the samples into the buffer
	for (auto it = samples.begin(); it != samples.end(); ++it) 
	{
		m_internalBuffer.push_back(*it);
	}
	
	// If internal buffer is full, send all of it to the hardware
	if (m_internalBuffer.size() == m_framesPerBuffer)
	{
		m_err = Pa_WriteStream(m_stream, m_internalBuffer.data(), m_framesPerBuffer);
		assert(m_err == paNoError);

		m_internalBuffer.clear();
	}
}

void Stream::Terminate()
{
	Pa_Terminate();
}

} //end namespace Core
} //end namespace Synt
