#include "cmn/LinearEnvelope.hpp"

namespace Synt
{
namespace Core
{

LinearEnvelope::LinearEnvelope(const LinearEnvelope::Params &params)
	: Stage(0, 1)
	, mPeakAmplitude(params.peakAmp)
	, mTotalSamples((long)((SyntConfig::GetInstance().sampleRate * SyntConfig::GetInstance().GetDuration()) + 0.5))
	, mGeneratedSamples(0)
	, mVolume(1.0f)
{
	mAttackSamples = static_cast<uint32_t>(params.atk * SyntConfig::GetInstance().sampleRate);
	if (mAttackSamples < 1)
		mAttackSamples = 1;

	mDecaySamples = static_cast<uint32_t>(params.dcy * SyntConfig::GetInstance().sampleRate);
	if (mDecaySamples < 1)
		mDecaySamples = 1;

	while ((mAttackSamples + mDecaySamples) >= mTotalSamples)
	{
		mAttackSamples--;
		mDecaySamples--;
	}

	mSustainSamples = mTotalSamples - (mAttackSamples + mDecaySamples);
}

void LinearEnvelope::Init()
{
	Seek();
}

bool LinearEnvelope::Work()
{
	// Currently a stage which isn't hooked up will assert...
	assert(mOutputs[0] != nullptr);
	auto outputBuf = mOutputs[0]->SharedBuffer();

	uint32_t decayStart = mTotalSamples - mDecaySamples;
	std::vector<float> localSamples;
	bool keepGenerating = true;
	long samplesToGenerate = outputBuf->GetBufferSize();

	if (mGeneratedSamples + samplesToGenerate > mTotalSamples)
		samplesToGenerate = mTotalSamples - mGeneratedSamples;

	if (samplesToGenerate == 0)
	{
		assert(localSamples.size() == 0);

		//Send termination value
		localSamples.push_back(FLT_MAX);
		keepGenerating = false;
	}
	
	while (--samplesToGenerate >= 0)
	{
		if (mGeneratedSamples < mAttackSamples || mGeneratedSamples > decayStart)
		{
			mVolume += mEnvelopeIncrement;
		}
		else if (mGeneratedSamples == mAttackSamples)
		{
			mVolume = mPeakAmplitude;
		}
		else if (mGeneratedSamples == mDecaySamples)
		{
			mEnvelopeIncrement = -mVolume / static_cast<AmplitudeVal>(mDecaySamples);
		}

		localSamples.push_back(mVolume);
		mGeneratedSamples++;
	}

	outputBuf->Push(localSamples);

	return keepGenerating;
}

void LinearEnvelope::Seek(AmplitudeVal currPhase /* = 0.0f */)
{
	//assert(0.0f < currPhase < 1.0f);
	uint32_t decayStart = mTotalSamples - mDecaySamples;

	mGeneratedSamples = static_cast<uint32_t>(currPhase * 
		SyntConfig::GetInstance().sampleRate * SyntConfig::GetInstance().duration);
	
	if (mGeneratedSamples < mAttackSamples)
	{
		mEnvelopeIncrement = mPeakAmplitude / static_cast<AmplitudeVal>(mAttackSamples);
		mVolume = static_cast<float>(mGeneratedSamples * mEnvelopeIncrement);
	}
	else if (mGeneratedSamples >= decayStart)
	{
		mEnvelopeIncrement = (-1 * mPeakAmplitude) / static_cast<AmplitudeVal>(mDecaySamples);
		mVolume = mPeakAmplitude + static_cast<AmplitudeVal>((mDecaySamples - mGeneratedSamples) * mEnvelopeIncrement);
	}
	else
	{
		mVolume = mPeakAmplitude;
	}
}

} //end namespace Core
} //end namespace Synt
