#include "cmn/AMSource.hpp"
#include <limits>

namespace Synt
{
namespace Core
{

AMSource::AMSource(const AMSource::Params &params)
	: mNoteFreq(SyntConfig::GetInstance().GetFrequency(params.noteId))
	, mModulatorFreq(mNoteFreq * params.modMult)
	, mModulatorIncrement(SyntConfig::GetInstance().frequencyRad * mModulatorFreq)
	, mCarrierIncrement(SyntConfig::GetInstance().frequencyRad * mNoteFreq)
	, mModulatorPhase(0.0f)
	, mCarrierPhase(0.0f)
	, mModulatorAmplitude(params.sidebandAmplitude) 
	, mModulatorScale(1 / (1 + mModulatorAmplitude))
{
}
//
// Original Algorithm true AM :
//
// f(t) = (Ac * sin(Wc * t) * (1 + Am * sin(Wm * t))) / (1 + Am)
// Ac = Modulator Frequency : Determines the peak amplitude of the carrier
// Am = Modulator Amplitude : Determines the peak amplitude of the modulator
// Wc, Wm = Frequencies expressed in radians
// 
// The modulator has to be offset to produce positive values, otherwise AM will
// ignore the modulator component of f(t)
//
void AMSource::Fill(std::vector<AmplitudeVal> &samplesBuf, const long samplesToGenerate)
{
	for (long samples = 0; samples < samplesToGenerate; samples++)
	{
		AmplitudeVal modulatorPortion = 1.0f + (mModulatorAmplitude * sin(mModulatorPhase));
		samplesBuf.push_back((sin(mCarrierPhase) * modulatorPortion) * mModulatorScale);
		mCarrierPhase += mCarrierIncrement;
		mModulatorPhase += mModulatorIncrement;

		if (mCarrierPhase >= TWO_PI)
			mCarrierPhase -= TWO_PI;
		
		if (mModulatorPhase >= TWO_PI)
			mModulatorPhase -= TWO_PI;
	}
}

} //end namespace Core
} //end namespace Synt
