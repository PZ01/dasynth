#include "cmn/FMSource.hpp"
#include <limits>

namespace Synt
{
namespace Core
{

FMSource::FMSource(const FMSource::Params &params)
	: mNoteFreq(SyntConfig::GetInstance().GetFrequency(params.noteId))
	, mModulatorFreq(mNoteFreq * params.modMult)
	, mModulatorIncrement(SyntConfig::GetInstance().frequencyRad * mModulatorFreq)
	, mModulatorPhase(0.0f)
	, mCarrierIncrement(0.0f)
	, mCarrierPhase(0.0f)
	, mModulatorAmplitude(mModulatorFreq * params.beta) // Am = Fm * B(Index of Modulation)
	, mVolume(1.0f)
{
}
//
// Original Algorithm for FM :
//
// f(t) = Ac * sin(Wc * t + (Am * sin(Wm * t))
// Am = Modulator Amplitude : Determines the peak variation in frequency of the signal
// Wm = Modulator Frequency : Determines the rate at which the frequency changes.
// 
// The modulator frequency is usually specified as a multiple of the carrier and identified
// by the c:m ratio. Integer multiples produce a harmonic spectrum.
void FMSource::Fill(std::vector<AmplitudeVal> &samplesBuf, const long samplesToGenerate)
{
	PhaseAccumulator frequencyRad = SyntConfig::GetInstance().frequencyRad;
	for (long samples = 0; samples < samplesToGenerate; samples++)
	{
		samplesBuf.push_back(mVolume * sin(mCarrierPhase));
		AmplitudeVal modulatorPortion = mModulatorAmplitude * sin(mModulatorPhase);
		mCarrierIncrement = frequencyRad * (mNoteFreq + modulatorPortion);
		mCarrierPhase += mCarrierIncrement;
		mModulatorPhase += mModulatorIncrement;

		if (mCarrierPhase >= TWO_PI)
			mCarrierPhase -= TWO_PI;
		
		if (mModulatorPhase >= TWO_PI)
			mModulatorPhase -= TWO_PI;
	}
}

} //end namespace Core
} //end namespace Synt
